package com.example.htdk_shop.model;

import java.io.Serializable;

public class InvoiceDetail implements Serializable {

    private int id;
    private String image;
    private String name;
    private int qty;
    private int price;

    public InvoiceDetail(int id, String image, String name, int qty, int price) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.qty = qty;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}
