package com.example.htdk_shop.ultil;

public class Server {
    public static String localhost = "api.loopdu.com";

    //Lay id va ten loai san pham
    public static String Categories = "https://" + localhost + "/categorie.php";

    //Lay san pham moi nhat va gia thap nhat
    public static String NewProduct = "https://" + localhost + "/product_new.php";
    public static String LowPriceProduct = "https://" + localhost + "/product_lowprice.php";

    //Lay danh sach san pham theo loai hang tu server xuong
    public static String Smartphone = "https://" + localhost + "/smartphone.php";
    public static String Tablet = "https://" + localhost + "/tablet.php";
    public static String SmartWatch = "https://" + localhost + "/smartwatch.php";
    public static String Laptop = "https://" + localhost + "/laptop.php";

    //Cap nhat don hang va chi tiet don hang len server
    public static String Invoice_POST = "https://" + localhost + "/invoice_post.php";
    public static String Invoice_Detail_POST = "https://" + localhost + "/invoice_detail_post.php";

    //Lay thong tin don hang va chi tiet don hang tu server xuong
    public static String Invoice_GET= "https://" + localhost + "/invoice_get.php";
    public static String Invoice_Detail_GET= "https://" + localhost + "/invoice_detail_get.php";

    //API dang ky và dang nhap
    public static String Register = "https://" + localhost + "/register.php";
    public static String Login = "https://" + localhost + "/login.php";

    //Lay thong tin khach hang tu server
    public static String CustomerInfo_GET = "https://" + localhost + "/customerinfo_get.php";

}
