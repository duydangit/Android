package com.example.htdk_shop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.htdk_shop.R;
import com.example.htdk_shop.adapter.ProductAdapter;
import com.example.htdk_shop.model.Cart;
import com.example.htdk_shop.model.Product;
import com.example.htdk_shop.ultil.CheckConnection;
import com.example.htdk_shop.ultil.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.htdk_shop.R.menu.main_toolbar;

public class MainActivity extends AppCompatActivity {

    Toolbar tbarMain;
    ImageView imvLinkSmartWatch, imvLinkSmartphone, imvLinkLaptop, imvLinkTablet, imvLinkMain, imvLinkContact, imvLinkInfo;

    RecyclerView recMainProductNew;
    RecyclerView recMainProductLowPrice;

    ArrayList <Product> listProductNew;
    ArrayList <Product> listProductLowPrice;

    ProductAdapter productAdapterNew;
    ProductAdapter productAdapterLowPrice;

    public static ArrayList<Cart> manggiohang;
    public static String username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linksView();
        addEvents();
        if (CheckConnection.haveNetworkConnection(getApplicationContext())){
                ActionBar();
                getDataProductNew();
                getDataProductLowPrice();
        } else {
            CheckConnection.ShowToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
            finish();
        }

    }

    private void linksView(){
        tbarMain = findViewById(R.id.tbarMain);
        imvLinkMain = findViewById(R.id.imvLinkMain);
        imvLinkContact = findViewById(R.id.imvLinkContact);
        imvLinkInfo = findViewById(R.id.imvLinkInfo);
        imvLinkSmartphone = findViewById(R.id.imvLinkSmartphone);
        imvLinkTablet = findViewById(R.id.imvLinkTablet);
        imvLinkSmartWatch = findViewById(R.id.imvLinkSmartWatch);
        imvLinkLaptop = findViewById(R.id.imvLinkLaptop);
        recMainProductNew = findViewById(R.id.recMainProductNew);
        recMainProductLowPrice = findViewById(R.id.recMainProductLowPrice);

        // New Product
        listProductNew = new ArrayList<>();
        productAdapterNew = new ProductAdapter(MainActivity.this, R.layout.item_product_new, listProductNew);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setSmoothScrollbarEnabled(true);
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recMainProductNew.setHasFixedSize(true);
        recMainProductNew.setLayoutManager(layoutManager);
        recMainProductNew.setAdapter(productAdapterNew);


        listProductLowPrice = new ArrayList<>();
        productAdapterLowPrice = new ProductAdapter(MainActivity.this, R.layout.item_product_lowprice, listProductLowPrice);
        recMainProductLowPrice.setHasFixedSize(true);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(this);
        layoutManager2.setSmoothScrollbarEnabled(true);
        layoutManager2.setOrientation(RecyclerView.HORIZONTAL);
        recMainProductLowPrice.setLayoutManager(layoutManager2);
        recMainProductLowPrice.setAdapter(productAdapterLowPrice);


        if(manggiohang == null){
            manggiohang = new ArrayList<Cart>();
        }
    }
    private void addEvents() {
        imvLinkMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckConnection.haveNetworkConnection(getApplicationContext())){
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(intent);
                }else{
                    CheckConnection.ShowToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
                }
            }
        });
        imvLinkContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckConnection.haveNetworkConnection(getApplicationContext())){
                    Intent intent = new Intent(MainActivity.this, ContactActivity.class);
                    startActivity(intent);
                }else{
                    CheckConnection.ShowToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
                }
            }
        });
        imvLinkInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckConnection.haveNetworkConnection(getApplicationContext())){
                    Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                    startActivity(intent);
                }else{
                    CheckConnection.ShowToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
               }
           }
        });
        imvLinkSmartWatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckConnection.haveNetworkConnection(getApplicationContext())){
                    Intent intent = new Intent(MainActivity.this, SmartWatchActivity.class);
                    startActivity(intent);
                }else{
                    CheckConnection.ShowToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
                }
            }
        });
        imvLinkSmartphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckConnection.haveNetworkConnection(getApplicationContext())){
                    Intent intent = new Intent(MainActivity.this, SmartphoneActivity.class);
                    startActivity(intent);
                }else{
                    CheckConnection.ShowToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
                }
            }
        });
        imvLinkTablet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckConnection.haveNetworkConnection(getApplicationContext())){
                    Intent intent = new Intent(MainActivity.this, TabletActivity.class);
                    startActivity(intent);
                }else{
                    CheckConnection.ShowToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
                }
            }
        });
        imvLinkLaptop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckConnection.haveNetworkConnection(getApplicationContext())){
                    Intent intent = new Intent(MainActivity.this, LaptopActivity.class);
                    startActivity(intent);
                }else{
                    CheckConnection.ShowToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
                }
            }
        });
    }
    private void ActionBar() {
        setSupportActionBar(tbarMain);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }
    private void getDataProductNew() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArray = new JsonArrayRequest(Request.Method.GET, Server.NewProduct, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i=0; i<response.length(); i++){
                    try {
                        JSONObject jsonProduct = response.getJSONObject(i);
                        int id = jsonProduct.getInt("product_id");
                        String name = jsonProduct.getString("product_name");
                        int price = jsonProduct.getInt("product_price");
                        String image = jsonProduct.getString("product_image");
                        String info = jsonProduct.getString("product_describe");
                        int categoryId = jsonProduct.getInt("category_id");
                        listProductNew.add(new Product(id,name,price,image,info,categoryId));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                productAdapterNew.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArray);
    }
    private void getDataProductLowPrice() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArray = new JsonArrayRequest(Request.Method.GET, Server.LowPriceProduct, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i=0;i<response.length();i++){
                    try {
                        JSONObject jsonProduct = response.getJSONObject(i);
                        int id = jsonProduct.getInt("product_id");
                        String name = jsonProduct.getString("product_name");
                        int price = jsonProduct.getInt("product_price");
                        String image = jsonProduct.getString("product_image");
                        String info = jsonProduct.getString("product_describe");
                        int categoryId = jsonProduct.getInt("category_id");
                        listProductLowPrice.add(new Product(id,name,price,image,info,categoryId));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                productAdapterLowPrice.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                   public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
       requestQueue.add(jsonArray);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(main_toolbar,menu);

        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if(item.getItemId() == R.id.UserLogin){
            if(username == null){
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }else{
                Toast.makeText(getApplicationContext(), "Bạn đã login rồi, không cần login lại!", Toast.LENGTH_SHORT).show();
            }
        }
        if(item.getItemId() == R.id.UserRegister){
            startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
        }
        if(item.getItemId() == R.id.UserInvoices){
            startActivity(new Intent(getApplicationContext(), InvoiceActivity.class));
        }

        if(item.getItemId() == R.id.menuCart){
            startActivity(new Intent(getApplicationContext(), CartActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }
}