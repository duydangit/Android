package com.example.htdk_shop.activity;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;

import com.example.htdk_shop.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class ContactActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    Toolbar tbarContact;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapInfo);
        mapFragment.getMapAsync(this);

        linkViews();
        actionBar();
    }

    private void linkViews() {
        tbarContact = findViewById(R.id.tbarContact);
    }

    private void actionBar() {
        setSupportActionBar(tbarContact);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tbarContact.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng hochiminh = new LatLng(10.855941, 106.785007);
        mMap.addMarker(new MarkerOptions().position(hochiminh).title("HHTDKB Mobile Shop").snippet("Nơi cá Koi bơi lội tung tăng :))")).setIcon(BitmapDescriptorFactory.defaultMarker());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hochiminh,18));
    }
}
