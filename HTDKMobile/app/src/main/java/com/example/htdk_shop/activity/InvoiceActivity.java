package com.example.htdk_shop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.htdk_shop.R;
import com.example.htdk_shop.adapter.InvoiceAdapter;
import com.example.htdk_shop.model.Invoice;
import com.example.htdk_shop.ultil.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class InvoiceActivity extends AppCompatActivity {

    Toolbar tbarInvoice;
    TextView txbInvoiceNotification;
    ImageView imvInvoiceNotification;
    RecyclerView recInvoice;

    com.example.htdk_shop.adapter.InvoiceAdapter InvoiceAdapter;
    ArrayList<Invoice> listInvoice;

    String username = MainActivity.username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);

        linksView();
        addEvents();
        ActionToolbar();
        getData();
    }
    private void linksView() {
        tbarInvoice= findViewById(R.id.tbarInvoice);
        txbInvoiceNotification = findViewById(R.id.txbInvoiceNotification);
        imvInvoiceNotification = findViewById(R.id.imvInvoiceNotification);
        recInvoice =  findViewById(R.id.recInvoice);

        listInvoice = new ArrayList<>();
        InvoiceAdapter = new InvoiceAdapter(InvoiceActivity.this, R.layout.item_invoice, listInvoice);
        recInvoice.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setSmoothScrollbarEnabled(true);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recInvoice.setLayoutManager(layoutManager);
        recInvoice.setAdapter(InvoiceAdapter);
    }
    private void addEvents() {
    }
    private void ActionToolbar() {
        setSupportActionBar(tbarInvoice);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tbarInvoice.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });
    }
    private void getData() {
        if(username == null){
            Intent intent = new Intent(InvoiceActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
        else{
             RequestQueue requestQueue = Volley.newRequestQueue(this);
            JsonArrayRequest jsonArray = new JsonArrayRequest(Request.Method.GET, Server.Invoice_GET + "?user="+ username, null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    txbInvoiceNotification.setVisibility(View.INVISIBLE);
                    imvInvoiceNotification.setVisibility(View.INVISIBLE);
                    recInvoice.setVisibility(View.VISIBLE);
                    for(int i = 0; i < response.length(); i++){
                        try {
                            JSONObject jsonProduct = response.getJSONObject(i);
                            int id = jsonProduct.getInt("invoice_id");
                            String user = jsonProduct.getString("customer_user");
                            String name = jsonProduct.getString("customer_name");
                            int phone = jsonProduct.getInt("customer_phone");
                            String email = jsonProduct.getString("customer_email");
                            String address = jsonProduct.getString("customer_address");
                            int status = jsonProduct.getInt("invoice_status");
                            String date = jsonProduct.getString("invoice_date");

                            listInvoice.add(new Invoice(id,user,name,phone,email,address,status,date));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    InvoiceAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            txbInvoiceNotification.setVisibility(View.VISIBLE);
                            imvInvoiceNotification.setVisibility(View.VISIBLE);
                            recInvoice.setVisibility(View.INVISIBLE);
                            //Toast.makeText(InvoiceActivity.this, "Không tồn tại đơn hàng!", Toast.LENGTH_SHORT).show();
                            //Toast.makeText(InvoiceActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
            );
            requestQueue.add(jsonArray);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_toolbar,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menuCart:
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}