package com.example.htdk_shop.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.htdk_shop.R;
import com.example.htdk_shop.activity.InvoiceDetailActivity;
import com.example.htdk_shop.model.Invoice;

import java.io.Serializable;
import java.util.ArrayList;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.Viewholder> {

    private Context context;
    private int layoutItemInvoice;
    private ArrayList<Invoice> listInvoice;

    public InvoiceAdapter(Context context, int layoutItemInvoice, ArrayList<Invoice> listInvoice) {
        this.context = context;
        this.layoutItemInvoice = layoutItemInvoice;
        this.listInvoice = listInvoice;
    }
    public class Viewholder extends RecyclerView.ViewHolder {
        ImageView imvItemInvoice;
        TextView txtItemInvoiceId, txtItemInvoiceDate, txtItemInvoiceStatus;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            imvItemInvoice = itemView.findViewById(R.id.imvItemInvoice);
            txtItemInvoiceId = itemView.findViewById(R.id.txtItemInvoiceId);
            txtItemInvoiceDate= itemView.findViewById(R.id.txtItemInvoiceDate);
            txtItemInvoiceStatus = itemView.findViewById(R.id.txtItemInvoiceStatus);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Invoice invoice = listInvoice.get(getAdapterPosition());
                    //Toast.makeText(view.getContext(), "Tính năng chi tiết đơn hàng đang phát triển!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, InvoiceDetailActivity.class); //fix lai sau nhe Duy
                    intent.putExtra("Invoice", (Serializable) invoice);
                    context.startActivity(intent);
                }
            });
        }
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(layoutItemInvoice, parent, false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        Invoice invoice = listInvoice.get(position);

        String[] date = new String[2];
        date = invoice.getDate().split(" ");
        String[] daymonth = date[0].split("-");

        holder.imvItemInvoice.setImageResource(R.drawable.invoice_200px);
        holder.txtItemInvoiceId.setText("Mã đơn hàng: #" + invoice.getId()+daymonth[2]+daymonth[1]);

        holder.txtItemInvoiceDate.setText(date[1] + " | " +  date[0]);

        if(invoice.getStatus() == 0)
        {
            holder.txtItemInvoiceStatus.setText("Đang chờ duyệt");
        }
        else if(invoice.getStatus() == 1){
            holder.txtItemInvoiceStatus.setText("Đã duyệt");
        }

    }

    @Override
    public int getItemCount() {
        return listInvoice.size();
    }

}
