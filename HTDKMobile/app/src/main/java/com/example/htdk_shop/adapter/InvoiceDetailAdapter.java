package com.example.htdk_shop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.htdk_shop.R;
import com.example.htdk_shop.model.InvoiceDetail;
import com.example.htdk_shop.ultil.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class InvoiceDetailAdapter extends RecyclerView.Adapter<InvoiceDetailAdapter.ViewHolder> {

    Context context;
    int layoutItemInvoiceDetail;
    ArrayList<InvoiceDetail> listInvoiceDetailProduct;


    public InvoiceDetailAdapter(Context context, int layoutItemInvoiceDetail, ArrayList<InvoiceDetail> listInvoiceDetailProduct) {
        this.context = context;
        this.layoutItemInvoiceDetail = layoutItemInvoiceDetail;
        this.listInvoiceDetailProduct = listInvoiceDetailProduct;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imvItemInvoiceDetail;
        TextView txtItemInvoiceDetailName, txtItemInvoiceDetailQty, txtItemInvoiceDetailPrice;

        private ItemClickListener itemClickListener;

        public ViewHolder(View itemView) {
            super(itemView);
            imvItemInvoiceDetail = (ImageView) itemView.findViewById(R.id.imvItemInvoiceDetailImage);
            txtItemInvoiceDetailName = (TextView) itemView.findViewById(R.id.txtItemInvoiceDetailName);
            txtItemInvoiceDetailPrice = (TextView) itemView.findViewById(R.id.txtItemInvoiceDetailPrice);
            txtItemInvoiceDetailQty = (TextView) itemView.findViewById(R.id.txtItemInvoiceDetailQty);
        }

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(layoutItemInvoiceDetail, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final InvoiceDetail product = listInvoiceDetailProduct.get(position);

        Picasso.with(context).load(product.getImage()).into(holder.imvItemInvoiceDetail);
        holder.txtItemInvoiceDetailName.setText(product.getName());
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        holder.txtItemInvoiceDetailPrice.setText("Giá : " + decimalFormat.format(product.getPrice())+ " đ");
        holder.txtItemInvoiceDetailQty.setText("Số lượng: "+product.getQty());

    }
    @Override
    public int getItemCount() {
        return listInvoiceDetailProduct.size();
    }
}
