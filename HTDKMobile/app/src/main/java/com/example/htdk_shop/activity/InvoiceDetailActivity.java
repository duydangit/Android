package com.example.htdk_shop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.htdk_shop.R;
import com.example.htdk_shop.adapter.InvoiceDetailAdapter;
import com.example.htdk_shop.model.Invoice;
import com.example.htdk_shop.model.InvoiceDetail;
import com.example.htdk_shop.ultil.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class InvoiceDetailActivity extends AppCompatActivity {
    Toolbar tbarInvoiceDetail;
    RecyclerView recInvoiceDetail;
    TextView txtInvoiceDetailID,txtInvoiceDetailName, txtInvoiceDetailDate, txtInvoiceDetailTotal;
    Button btnInvoiceDetailCancel;

    Invoice invoice;
    ArrayList<InvoiceDetail> listInvoiceDetailProduct;
    InvoiceDetailAdapter invoiceDetailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_detail);

        linkView();
        ActionToolbar();
        addEvents();
        getInvoice();
    }

    private void linkView() {
        tbarInvoiceDetail = (Toolbar) findViewById(R.id.tbarInvoiceDetail);
        txtInvoiceDetailID = findViewById(R.id.txtInvoiceDetailID);
        txtInvoiceDetailName = findViewById(R.id.txtInvoiceDetailName);
        txtInvoiceDetailDate = findViewById(R.id.txtInvoiceDetailDate);
        recInvoiceDetail = findViewById(R.id.recInvoiceDetail);
        txtInvoiceDetailTotal = findViewById(R.id.txtInvoiceDetailTotal);
        btnInvoiceDetailCancel = findViewById(R.id.btnInvoiceDetailCancel);

        setSupportActionBar(tbarInvoiceDetail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Get Invoice Detail
        listInvoiceDetailProduct = new ArrayList<>();
        invoiceDetailAdapter = new InvoiceDetailAdapter(getApplicationContext(), R.layout.item_invoice_detail, listInvoiceDetailProduct);
        recInvoiceDetail.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setSmoothScrollbarEnabled(true);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recInvoiceDetail.setLayoutManager(layoutManager);
        recInvoiceDetail.setAdapter(invoiceDetailAdapter);

    }
    private void ActionToolbar() {
        setSupportActionBar(tbarInvoiceDetail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tbarInvoiceDetail.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(getApplicationContext(),InvoiceActivity.class);
                startActivity(intent);
            }
        });
    }
    private void addEvents() {
    }
    private void getInvoice() {
        invoice = (Invoice) getIntent().getSerializableExtra("Invoice");

        String[] date = new String[2];
        date = invoice.getDate().split(" ");
        String[] daymonth = date[0].split("-");

        txtInvoiceDetailID.setText("Mã đơn hàng: #" + invoice.getId()+daymonth[2]+daymonth[1]);
        txtInvoiceDetailName.setText("Tên khách hàng: " + invoice.getName());
        txtInvoiceDetailDate.setText("Ngày đặt: " + date[0]);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArray = new JsonArrayRequest(Request.Method.GET, Server.Invoice_Detail_GET+"?invoice_id="+invoice.getId(), null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                int Total = 0;
                for (int i=0; i<response.length(); i++){
                    try {
                        JSONObject jsonProduct = response.getJSONObject(i);
                        int id = jsonProduct.getInt("product_id");
                        String image = jsonProduct.getString("product_image");
                        String name = jsonProduct.getString("product_name");
                        int qty = jsonProduct.getInt("product_quantity");
                        int price = jsonProduct.getInt("product_price");

                        Total += price;
                        listInvoiceDetailProduct.add(new InvoiceDetail(id,image,name,qty,price));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
                txtInvoiceDetailTotal.setText(decimalFormat.format(Total) + "đ");

                invoiceDetailAdapter.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArray);
    }
}