package com.example.htdk_shop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.htdk_shop.R;
import com.example.htdk_shop.adapter.TabletAdapter;
import com.example.htdk_shop.model.Product;
import com.example.htdk_shop.ultil.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TabletActivity extends AppCompatActivity {

    Toolbar tbarTablet;
    RecyclerView recTablet;

    TabletAdapter tabletAdapter;
    ArrayList<Product> listTablet;

    int categoryId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablet);

        linksView();
        addEvents();
        ActionToolbar();
        getData();
        getCategoryId();
    }
    private void linksView() {
        tbarTablet = findViewById(R.id.tbarTablet);
        recTablet =  findViewById(R.id.recTablet);

        listTablet = new ArrayList<>();
        tabletAdapter = new TabletAdapter(TabletActivity.this, R.layout.item_tablet, listTablet);
        recTablet.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setSmoothScrollbarEnabled(true);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recTablet.setLayoutManager(layoutManager);
        recTablet.setAdapter(tabletAdapter);
    }
    private void addEvents() {
    }
    private void ActionToolbar() {
        setSupportActionBar(tbarTablet);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tbarTablet.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    private void getData() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArray = new JsonArrayRequest(Request.Method.GET, Server.Tablet, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for(int i = 0; i < response.length(); i++){
                    try {
                        JSONObject jsonProduct = response.getJSONObject(i);
                        int id = jsonProduct.getInt("product_id");
                        String name = jsonProduct.getString("product_name");
                        int price = jsonProduct.getInt("product_price");
                        String image = jsonProduct.getString("product_image");
                        String info = jsonProduct.getString("product_describe");
                        int categoryId = jsonProduct.getInt("category_id");
                        listTablet.add(new Product(id,name,price,image,info,categoryId));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                tabletAdapter.notifyDataSetChanged();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TabletActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArray);
    }
    private void getCategoryId() {
        categoryId = getIntent().getIntExtra("category_id", 2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_toolbar,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.UserLogin:
                if(MainActivity.username == null){
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }else{
                    Toast.makeText(getApplicationContext(), "Bạn đã login rồi, không cần login lại!", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.UserRegister:
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
                break;

            case R.id.UserInvoices:
                startActivity(new Intent(getApplicationContext(), InvoiceActivity.class));
                break;

            case R.id.menuCart:
                startActivity(new Intent(getApplicationContext(), CartActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}