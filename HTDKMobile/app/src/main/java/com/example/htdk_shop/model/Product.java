package com.example.htdk_shop.model;

import java.io.Serializable;

public class Product implements Serializable {
    private int id;
    private String name;
    private Integer price;
    private String image;
    private String info;
    private int categoryId;

    public Product(int id, String name, Integer price, String image, String info, int categoryId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.info = info;
        this.categoryId = categoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
