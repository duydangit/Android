package com.example.htdk_shop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.htdk_shop.R;
import com.example.htdk_shop.ultil.CheckConnection;
import com.example.htdk_shop.ultil.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText edtLoginUsername, edtLoginPassword;
    Button btnLogin,btnRegister, btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        linksView();
        addEvents();
    }

    private void linksView() {
        btnRegister = findViewById(R.id.btnRegister);
        edtLoginUsername = findViewById(R.id.edtLoginUsername);
        edtLoginPassword = findViewById(R.id.edtLoginPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnExit = findViewById(R.id.btnExit);

        Intent intent = getIntent();
        edtLoginUsername.setText(intent.getStringExtra("username"));
        edtLoginPassword.setText(intent.getStringExtra("password"));
    }
    private void addEvents() {
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkExistUser();
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    public void checkExistUser() {

            final String username = edtLoginUsername.getText().toString().trim();
            final String password = edtLoginPassword.getText().toString().trim();

        if (username.length() > 0 && password.length() > 0) {
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Server.Login, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    if (response.equals("1")) {
                        Toast.makeText(LoginActivity.this, "Đăng nhập thành công", Toast.LENGTH_LONG).show();
                        MainActivity.username = username;
                        MainActivity.password = password;
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Tài khoản không tồn tại hoặc username và password ko đúng", Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Lỗi: " + error.toString(), Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    JSONArray jsonArray = new JSONArray();
                    JSONObject jsonUser = new JSONObject();
                    try {
                        jsonUser.put("customer_user",username);
                        jsonUser.put("customer_pwd",password);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    jsonArray.put(jsonUser);

                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("login",jsonArray.toString());
                    return hashMap;
                }
            };
            requestQueue.add(stringRequest);
        } else {
            CheckConnection.ShowToast_Short(getApplicationContext(), "Vui lòng nhập đầy đủ thông tin");
        }
    }
}