package com.example.htdk_shop.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.htdk_shop.R;
import com.example.htdk_shop.activity.ProductDetailActivity;
import com.example.htdk_shop.model.Product;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class SmartWatchAdapter extends RecyclerView.Adapter<SmartWatchAdapter.Viewholder> {

    private Context context;
    private int layoutItemSmartWatch;
    private ArrayList<Product> listSmartWatch;

    public SmartWatchAdapter(Context context, int layoutItemSmartWatch, ArrayList<Product> listSmartWatch) {
        this.context = context;
        this.layoutItemSmartWatch = layoutItemSmartWatch;
        this.listSmartWatch = listSmartWatch;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        ImageView imvItemSmartWatch;
        TextView txtItemSmartWatchName, txtItemSmartWatchPrice, txtItemSmartWatchInfo;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            imvItemSmartWatch = itemView.findViewById(R.id.imvItemSmartWatch);
            txtItemSmartWatchName = itemView.findViewById(R.id.txtItemSmartWatchName);
            txtItemSmartWatchPrice = itemView.findViewById(R.id.txtItemSmartWatchPrice);
            txtItemSmartWatchInfo = itemView.findViewById(R.id.txtItemSmartWatchInfo);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Product product = listSmartWatch.get(getAdapterPosition());

                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra("Product", (Serializable) product);
                    context.startActivity(intent);
                }
            });
        }
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(layoutItemSmartWatch,parent,false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        Product product = listSmartWatch.get(position);

        Picasso.with(context).load(product.getImage()).into(holder.imvItemSmartWatch);
        holder.txtItemSmartWatchName.setText(product.getName());
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        holder.txtItemSmartWatchPrice.setText("Giá : " + decimalFormat.format(product.getPrice())+ " đ");
        holder.txtItemSmartWatchInfo.setText(product.getInfo());
    }

    @Override
    public int getItemCount() {
        return listSmartWatch.size();
    }
}
