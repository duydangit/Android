package com.example.htdk_shop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.htdk_shop.R;
import com.example.htdk_shop.model.Cart;
import com.example.htdk_shop.model.Product;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

public class ProductDetailActivity extends AppCompatActivity {

    Toolbar tbarProductDetail;

    ImageView imvProductDetail;
    TextView txtProductDetailName, txtProductDetailPrice, txtProductDetailInfo;
    Button btnAddCart;

    Product product;
    //Spinner spnQty;
   //Integer [] arrQty;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        linkView();
        addEvents();
        getDataProductDetail();
    }

    private void linkView() {

        tbarProductDetail = (Toolbar) findViewById(R.id.tbarProductDetail);
        imvProductDetail = (ImageView) findViewById(R.id.imvProductDetail);
        txtProductDetailName = (TextView) findViewById(R.id.txtProductDetailName);
        txtProductDetailPrice = (TextView) findViewById(R.id.txtProductDetailPrice);
        txtProductDetailInfo = (TextView) findViewById(R.id.txtProductDetailInfo);
        btnAddCart = (Button) findViewById(R.id.btnAddCart);

        setSupportActionBar(tbarProductDetail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*arrQty = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(getApplicationContext(), R.layout.spinner, R.id.textviewspinner, arrQty);
        adapter.setDropDownViewResource(R.layout.spinner);
        spnQty.setAdapter(adapter);*/
    }
    private void addEvents() {
        btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.manggiohang.size() > 0){
                    boolean exist = false;
                    for(int i = 0; i < MainActivity.manggiohang.size(); i++){
                        Log.d("ABC", MainActivity.manggiohang.get(i).getProductId() + MainActivity.manggiohang.get(i).getProductName()+"\t");
                        if(MainActivity.manggiohang.get(i).getProductId() == product.getId()){
                            //int qty = Integer.parseInt(spnQty.getSelectedItem().toString());
                            int qty = 1;
                            int qtynew = qty + MainActivity.manggiohang.get(i).getQty();
                            if(qtynew > 10){
                                MainActivity.manggiohang.get(i).setQty(10);
                                Toast.makeText(ProductDetailActivity.this,"Chỉ được mua tối đa 10 sản phẩm cho một mặt hàng!",Toast.LENGTH_SHORT).show();
                            }else {
                                MainActivity.manggiohang.get(i).setQty(qtynew);
                            }
                            exist = true;
                        }
                    }
                    if(!exist){
                        //int qty = Integer.parseInt(spnQty.getSelectedItem().toString());
                        int qty = 1;
                        Cart cart = new Cart(product.getId(), product.getName(), product.getImage(), product.getPrice(), qty);
                        MainActivity.manggiohang.add(cart);
                    }

                }else {
                    //int qty = Integer.parseInt(spnQty.getSelectedItem().toString());
                    int qty = 1;
                    Cart cart = new Cart(product.getId(), product.getName(), product.getImage(), product.getPrice(), qty);
                    MainActivity.manggiohang.add(cart);
                }

                Intent intent = new Intent(ProductDetailActivity.this, CartActivity.class);
                startActivity(intent);
            }
        });
        tbarProductDetail.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getDataProductDetail() {
        Intent intent = getIntent();
        product = (Product) intent.getSerializableExtra("Product");
        Picasso.with(getApplicationContext()).load(product.getImage()).into(imvProductDetail);
        txtProductDetailName.setText(product.getName());
        DecimalFormat decimalformat = new DecimalFormat("###,###,###");
        txtProductDetailPrice.setText(decimalformat.format(product.getPrice())+" đ");
        txtProductDetailInfo.setText(product.getInfo());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_toolbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menuCart){
            startActivity(new Intent(ProductDetailActivity.this, CartActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}