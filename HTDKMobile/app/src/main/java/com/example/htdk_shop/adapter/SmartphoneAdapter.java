package com.example.htdk_shop.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.htdk_shop.R;
import com.example.htdk_shop.activity.ProductDetailActivity;
import com.example.htdk_shop.model.Product;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class SmartphoneAdapter extends RecyclerView.Adapter<SmartphoneAdapter.Viewholder> {

    private Context context;
    private int layoutItemSmartphone;
    private ArrayList<Product> listSmartphone;

    public SmartphoneAdapter(Context context, int layoutItemSmartphone, ArrayList<Product> listSmartphone) {
        this.context = context;
        this.layoutItemSmartphone = layoutItemSmartphone;
        this.listSmartphone = listSmartphone;
    }
    public class Viewholder extends RecyclerView.ViewHolder {
        ImageView imvItemSmartphone;
        TextView txtItemSmartphoneName, txtItemSmartphonePrice, txtItemSmartphoneInfo;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            imvItemSmartphone = itemView.findViewById(R.id.imvItemSmartphone);
            txtItemSmartphoneName = itemView.findViewById(R.id.txtItemSmartphoneName);
            txtItemSmartphonePrice = itemView.findViewById(R.id.txtItemSmartphonePrice);
            txtItemSmartphoneInfo = itemView.findViewById(R.id.txtItemSmartphoneInfo);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Product product = listSmartphone.get(getAdapterPosition());

                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra("Product", (Serializable) product);
                    context.startActivity(intent);
                }
            });
        }
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(layoutItemSmartphone, parent, false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        Product product = listSmartphone.get(position);

        Picasso.with(context).load(product.getImage()).into(holder.imvItemSmartphone);
        holder.txtItemSmartphoneName.setText(product.getName());
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        holder.txtItemSmartphonePrice.setText("Giá : " + decimalFormat.format(product.getPrice())+ " đ");
        holder.txtItemSmartphoneInfo.setText(product.getInfo());
    }

    @Override
    public int getItemCount() {
        return listSmartphone.size();
    }

}
