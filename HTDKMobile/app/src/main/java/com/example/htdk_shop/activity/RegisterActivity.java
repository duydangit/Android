package com.example.htdk_shop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.htdk_shop.R;
import com.example.htdk_shop.ultil.CheckConnection;
import com.example.htdk_shop.ultil.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    EditText edtRegisterUsername, edtRegisterPassword, edtRegisterName, edtRegisterPhoneNumber, edtRegisterEmail, edtRegisterAddress;
    Button btnRegisterOk, btnRegisterExit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        linksView();
        addEvents();
    }

    private void linksView() {
        edtRegisterUsername = findViewById(R.id.edtRegisterUsername);
        edtRegisterPassword = findViewById(R.id.edtRegisterPassword);
        edtRegisterName = findViewById(R.id.edtRegisterName);
        edtRegisterPhoneNumber = findViewById(R.id.edtRegisterPhoneNumber);
        edtRegisterEmail = findViewById(R.id.edtRegisterEmail);
        edtRegisterAddress = findViewById(R.id.edtRegisterAddress);
        btnRegisterOk = findViewById(R.id.btnRegisterOK);
        btnRegisterExit = findViewById(R.id.btnRegisterExit);
    }
    private void addEvents() {
        btnRegisterOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendData();
            }
        });
        btnRegisterExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    public void sendData(){
        final String username = edtRegisterUsername.getText().toString().trim();
        final String password = edtRegisterPassword.getText().toString().trim();
        final String name = edtRegisterName.getText().toString().trim();
        final String phone = edtRegisterPhoneNumber.getText().toString().trim();
        final String email = edtRegisterEmail.getText().toString().trim();
        final String address = edtRegisterAddress.getText().toString().trim();

        if(username.length() > 0 && password.length() > 0 && name.length() > 0 && phone.length() > 0 && email.length() > 0 && address.length() > 0)
        {
            CheckConnection.ShowToast_Short(getApplicationContext(),"Đang xử lý, vui lòng đợi trong giây lát!");
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Server.Register                                                                                                                                         , new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    Log.d("Register",response);

                    if (response.equals("1")) {
                        Toast.makeText(getApplicationContext(), "Chúc mừng, bạn đã tạo tài khoản thành công!",Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                        intent.putExtra("username",username);
                        intent.putExtra("password",password);
                        finish();
                        startActivity(intent);
                    }else {
                        Toast.makeText(getApplicationContext(), "Tạo tài khoản không thành công, username có thể đã tồn tại!",Toast.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),"Lỗi: "+ error.toString(),Toast.LENGTH_LONG);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    JSONObject jsonObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray();

                    try{
                        jsonObject.put("customer_name",name);
                        jsonObject.put("customer_phone",phone);
                        jsonObject.put("customer_email",email);
                        jsonObject.put("customer_user",username);
                        jsonObject.put("customer_pwd",password);
                        jsonObject.put("customer_perm","guest");
                        jsonObject.put("customer_address",address);

                        jsonArray.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    HashMap<String,String> hashMap = new HashMap<String, String>();
                    hashMap.put("register",jsonArray.toString());
                    return hashMap;
                }
            };
            requestQueue.add(stringRequest);
        }else {
            CheckConnection.ShowToast_Short(getApplicationContext(),"Vui lòng nhập đầy đủ thông tin");
        }
    }
}