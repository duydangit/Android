package com.example.htdk_shop.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.htdk_shop.R;
import com.example.htdk_shop.activity.CartActivity;
import com.example.htdk_shop.activity.MainActivity;
import com.example.htdk_shop.activity.ProductDetailActivity;
import com.example.htdk_shop.model.Cart;
import com.example.htdk_shop.ultil.ItemClickListener;
import com.example.htdk_shop.model.Product;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    Context context;
    int layoutItemProduct;
    ArrayList<Product> listProduct;


    public ProductAdapter(Context context, int layoutItemProduct, ArrayList<Product> listProduct) {
        this.context = context;
        this.layoutItemProduct = layoutItemProduct;
        this.listProduct = listProduct;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        ImageView imvItemProduct;
        TextView txtItemProductName,txtItemProductPrice;

        private ItemClickListener itemClickListener;

        public ViewHolder(View itemView) {
            super(itemView);
            imvItemProduct = (ImageView) itemView.findViewById(R.id.imvItemProduct);
            txtItemProductPrice = (TextView) itemView.findViewById(R.id.txtItemProductPrice);
            txtItemProductName = (TextView) itemView.findViewById(R.id.txtItemProductName);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public  void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }
        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            itemClickListener.onClick(view, getAdapterPosition(), true);
            return true;
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(layoutItemProduct, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Product product = listProduct.get(position);

        Picasso.with(context).load(product.getImage()).into(holder.imvItemProduct);
        holder.txtItemProductName.setText(product.getName());
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        holder.txtItemProductPrice.setText("Giá : " + decimalFormat.format(product.getPrice())+ " đ");

        holder.setItemClickListener(new ItemClickListener() {
            ImageView imvProductDetail;
            TextView txtProductDetailName, txtProductDetailPrice, txtProductDetailInfo;
            Button btnAddCart;
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if(isLongClick){
                    //Đang test dialog product
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View productdetailView = inflater.inflate(R.layout.activity_product_detail,null);

                    //Link view dialog
                    imvProductDetail = productdetailView.findViewById(R.id.imvProductDetail);
                    txtProductDetailName = productdetailView.findViewById(R.id.txtProductDetailName);
                    txtProductDetailPrice = productdetailView.findViewById(R.id.txtProductDetailPrice);
                    txtProductDetailInfo = productdetailView.findViewById(R.id.txtProductDetailInfo);
                    btnAddCart = productdetailView.findViewById(R.id.btnAddCart);

                    //Load product's info into Product Detail Dialog
                    Picasso.with(context).load(product.getImage()).into(imvProductDetail);
                    txtProductDetailName.setText(product.getName());
                    DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
                    txtProductDetailPrice.setText("Giá : " + decimalFormat.format(product.getPrice())+ " đ");
                    txtProductDetailInfo.setText(product.getInfo());
                    btnAddCart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(MainActivity.manggiohang.size() > 0){
                                boolean exist = false;
                                for(int i = 0; i < MainActivity.manggiohang.size(); i++){
                                    Log.d("ABC", MainActivity.manggiohang.get(i).getProductId() + MainActivity.manggiohang.get(i).getProductName()+"\t");
                                    if(MainActivity.manggiohang.get(i).getProductId() == product.getId()){
                                        //int qty = Integer.parseInt(spnQty.getSelectedItem().toString());
                                        int qty = 1;
                                        int qtynew = qty + MainActivity.manggiohang.get(i).getQty();
                                        if(qtynew > 10){
                                            MainActivity.manggiohang.get(i).setQty(10);
                                            Toast.makeText(context,"Chỉ được mua tối đa 10 sản phẩm cho một mặt hàng!",Toast.LENGTH_SHORT).show();
                                        }else {
                                            MainActivity.manggiohang.get(i).setQty(qtynew);
                                        }
                                        exist = true;
                                    }
                                }
                                if(!exist){
                                    //int qty = Integer.parseInt(spnQty.getSelectedItem().toString());
                                    int qty = 1;
                                    Cart cart = new Cart(product.getId(), product.getName(), product.getImage(), product.getPrice(), qty);
                                    MainActivity.manggiohang.add(cart);
                                }

                            }else {
                                //int qty = Integer.parseInt(spnQty.getSelectedItem().toString());
                                int qty = 1;
                                Cart cart = new Cart(product.getId(), product.getName(), product.getImage(), product.getPrice(), qty);
                                MainActivity.manggiohang.add(cart);
                            }

                            Intent intent = new Intent(context, CartActivity.class);
                            context.startActivity(intent);
                        }
                    });

                    //init dialog with product's info
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setView(productdetailView);
                    alertDialog.setCancelable(true);

                    AlertDialog dialog = alertDialog.create();
                    dialog.show();
                }
                else{
                    Product product = listProduct.get(position);

                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra("Product", (Serializable) product);
                    context.startActivity(intent);
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return listProduct.size();
    }
}
