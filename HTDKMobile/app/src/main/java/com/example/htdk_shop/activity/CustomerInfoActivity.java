package com.example.htdk_shop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.htdk_shop.R;
import com.example.htdk_shop.ultil.CheckConnection;
import com.example.htdk_shop.ultil.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.example.htdk_shop.R.layout;

public class CustomerInfoActivity extends AppCompatActivity {

    EditText edtCustomerInfoName, edtCustomerInfoPhone, edtCustomerInfoEmail, edtCustomerInfoAddress;
    Button btnCustomerInfoConfirm, btnCustomerInfoBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_customer_info);
        linksView();
        addEvents();
        getCustomerInfo();
        //getCustomerInfo(MainActivity.username);
    }
    private void linksView() {
        edtCustomerInfoName = findViewById(R.id.edtCustomerInfoName);
        edtCustomerInfoPhone = findViewById(R.id.edtCustomerInfoPhone);
        edtCustomerInfoEmail = findViewById(R.id.edtCustomerInfoEmail);
        edtCustomerInfoAddress = findViewById(R.id.edtCustomerInfoAddress);
        btnCustomerInfoConfirm = findViewById(R.id.btnCustomerInfoConfirm);
        btnCustomerInfoBack = findViewById(R.id.btnCustomerInfoBack);

        /*Intent intent = getIntent();
        edtCustomerInfoName.setText(intent.getStringExtra("name"));
        edtCustomerInfoPhone.setText(intent.getStringExtra("phone"));
        edtCustomerInfoEmail.setText(intent.getStringExtra("email"));
        edtCustomerInfoAddress.setText(intent.getStringExtra("address"));*/

    }

    private void addEvents() {
        btnCustomerInfoConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                senData();
            }
        });
        btnCustomerInfoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    public void getCustomerInfo() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArray = new JsonArrayRequest(Request.Method.GET, Server.CustomerInfo_GET+"?user="+MainActivity.username, null, new Response.Listener<JSONArray>() {

            String name, email, address;
            int phone;
            @Override
            public void onResponse(JSONArray response) {
                for (int i=0; i<response.length(); i++){
                    try {
                        JSONObject jsonCustomer = response.getJSONObject(i);
                        name = jsonCustomer.getString("customer_name");
                        phone = jsonCustomer.getInt("customer_phone");
                        email = jsonCustomer.getString("customer_email");
                        address = jsonCustomer.getString("customer_address");

                        //Load thong tin khach hang len don hang
                        edtCustomerInfoName.setText(name);
                        edtCustomerInfoPhone.setText(String.valueOf(phone));
                        edtCustomerInfoEmail.setText(email);
                        edtCustomerInfoAddress.setText(address);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CustomerInfoActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(jsonArray);
    }
    public void senData(){
        final String name = edtCustomerInfoName.getText().toString().trim();
        final String phone = edtCustomerInfoPhone.getText().toString().trim();
        final String email = edtCustomerInfoEmail.getText().toString().trim();
        final String address = edtCustomerInfoAddress.getText().toString().trim();
        if(name.length() > 0 && phone.length() > 0 && email.length() > 0)
        {
            CheckConnection.ShowToast_Short(getApplicationContext(),"Đang gửi thông tin đến shop!");
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Server.Invoice_POST, new Response.Listener<String>() {
                @Override
                public void onResponse(final String orderID) {
                    Log.d("Order_ID",orderID);
                    RequestQueue requestQueue1 = Volley.newRequestQueue(getApplicationContext());
                    StringRequest stringRequest1 = new StringRequest(Request.Method.POST, Server.Invoice_Detail_POST, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (response.equals("1")) {
                                MainActivity.manggiohang.clear();
                                CheckConnection.ShowToast_Short(getApplicationContext(), "Đặt hàng thành công!");
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            } else {
                                CheckConnection.ShowToast_Short(getApplicationContext(),"Lỗi");
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(),"Lỗi: "+ error.toString(),Toast.LENGTH_LONG);
                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            JSONArray jsonArray = new JSONArray();
                            for(int i=0;i<MainActivity.manggiohang.size();i++){
                                JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("invoice_id",orderID);
                                    jsonObject.put("product_id",MainActivity.manggiohang.get(i).getProductId());
                                    jsonObject.put("product_image",MainActivity.manggiohang.get(i).getProductImage());
                                    jsonObject.put("product_name",MainActivity.manggiohang.get(i).getProductName());
                                    jsonObject.put("product_quantity",MainActivity.manggiohang.get(i).getQty());
                                    jsonObject.put("product_price",MainActivity.manggiohang.get(i).getProductPrice());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                jsonArray.put(jsonObject);
                            }
                            HashMap<String,String> hashMap = new HashMap<String, String>();
                            hashMap.put("json",jsonArray.toString());
                            return hashMap;
                        }
                    };
                    requestQueue1.add(stringRequest1);
                    //}
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(),"Lỗi: "+ error.toString(),Toast.LENGTH_LONG);
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String,String> hashMap = new HashMap<String, String>();
                    hashMap.put("customer_user",MainActivity.username);
                    hashMap.put("customer_name",name);
                    hashMap.put("customer_phone",phone);
                    hashMap.put("customer_email",email);
                    hashMap.put("customer_address",address);
                    hashMap.put("invoice_status","0");

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = new Date();
                    String tam = formatter.format(date).toString();

                    hashMap.put("invoice_date",tam);
                    return hashMap;
                }
            };
            requestQueue.add(stringRequest);
        }else {
            CheckConnection.ShowToast_Short(getApplicationContext(),"Vui lòng nhập đầy đủ thông tin");
        }
    }
}