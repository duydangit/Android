package com.example.htdk_shop.model;

public class User {
    private String customer_id;

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public User(String customer_id) {
        this.customer_id = customer_id;
    }
}
