package com.example.htdk_shop.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.htdk_shop.R;
import com.example.htdk_shop.activity.ProductDetailActivity;
import com.example.htdk_shop.model.Product;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class TabletAdapter extends RecyclerView.Adapter<TabletAdapter.Viewholder> {

    private Context context;
    private int layoutItemTablet;
    private ArrayList<Product> listTablet;

    public TabletAdapter(Context context, int layoutItemTablet, ArrayList<Product> listTablet) {
        this.context = context;
        this.layoutItemTablet = layoutItemTablet;
        this.listTablet = listTablet;
    }
    public class Viewholder extends RecyclerView.ViewHolder {
        ImageView imvItemTablet;
        TextView txtItemTabletName, txtItemTabletPrice, txtItemTabletInfo;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            imvItemTablet = itemView.findViewById(R.id.imvItemTablet);
            txtItemTabletName = itemView.findViewById(R.id.txtItemTabletName);
            txtItemTabletPrice = itemView.findViewById(R.id.txtItemTabletPrice);
            txtItemTabletInfo = itemView.findViewById(R.id.txtItemTabletInfo);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Product product = listTablet.get(getAdapterPosition());

                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra("Product", (Serializable) product);
                    context.startActivity(intent);
                }
            });
        }
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(layoutItemTablet, parent, false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        Product product = listTablet.get(position);

        Picasso.with(context).load(product.getImage()).into(holder.imvItemTablet);
        holder.txtItemTabletName.setText(product.getName());
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        holder.txtItemTabletPrice.setText("Giá : " + decimalFormat.format(product.getPrice())+ " đ");
        holder.txtItemTabletInfo.setText(product.getInfo());
    }

    @Override
    public int getItemCount() {
        return listTablet.size();
    }
}
