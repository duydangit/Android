package com.example.htdk_shop.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.htdk_shop.R;

public class InfoActivity extends AppCompatActivity{

    Toolbar tbarInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        actionBar();
    }

    private void actionBar() {
        tbarInfo = (Toolbar) findViewById(R.id.tbarInfo);
        setSupportActionBar(tbarInfo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tbarInfo.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}