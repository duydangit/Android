package com.example.htdk_shop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.htdk_shop.R;
import com.example.htdk_shop.adapter.CartAdapter;
import com.example.htdk_shop.ultil.CheckConnection;

import java.text.DecimalFormat;

public class CartActivity extends AppCompatActivity {

    Toolbar tbarCart;
    RecyclerView recCart;

    ImageView imvCartNotification;
    public static TextView txtCartNotification;

    static TextView txtCartTotal;
    Button btnCartCheckout, btnCartContinueBuy;

    public static CartAdapter cartAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        linksView();
        ActionToolbar();
        CheckData();
        EventUltil();
        addEvents();
    }
    private void linksView() {
        tbarCart = findViewById(R.id.tbarCart);
        recCart = findViewById(R.id.recCart);
        imvCartNotification = findViewById(R.id.imvCartNotification);
        txtCartNotification = findViewById(R.id.txtCartNotification);
        txtCartTotal = findViewById(R.id.txtCartTotal);
        btnCartCheckout = findViewById(R.id.btnCartCheckout);
        //btnCartContinueBuy = findViewById(R.id.btnCartContinueBuy);

        cartAdapter = new CartAdapter(CartActivity.this,R.layout.item_cart, MainActivity.manggiohang);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setSmoothScrollbarEnabled(true);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        recCart.setHasFixedSize(true);
        recCart.setLayoutManager(layoutManager);
        recCart.setAdapter(cartAdapter);
    }

    private void addEvents() {

        btnCartCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MainActivity.username == null)
                {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }
                else{
                    if ( MainActivity.manggiohang.size() > 0 ){
                        Intent intent = new Intent(getApplicationContext(), CustomerInfoActivity.class);
                        startActivity(intent);
                    }else{
                        CheckConnection.ShowToast_Short(getApplicationContext(),"Giỏ hàng của bạn chưa có sản phẩm để thanh toán");
                    }
                }
            }
        });
        /*btnCartContinueBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CartActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });*/
    }
    private void ActionToolbar() {
        setSupportActionBar(tbarCart);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tbarCart.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(CartActivity.this,MainActivity.class);
                Toast.makeText(CartActivity.this, "Mời bạn tiếp tục mua hàng!", Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });
    }
    public static void EventUltil() {
        long total = 0;
        for (int i = 0; i<MainActivity.manggiohang.size();i++){
            total += MainActivity.manggiohang.get(i).Total();
        }
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        txtCartTotal.setText(decimalFormat.format(total) + "đ");
    }
    private void CheckData() {
        if (MainActivity.manggiohang.size()<=0){
            cartAdapter.notifyDataSetChanged();
            imvCartNotification.setVisibility(View.VISIBLE);
            txtCartNotification.setVisibility(View.VISIBLE);
            recCart.setVisibility(View.INVISIBLE);
        }else{
            cartAdapter.notifyDataSetChanged();
            imvCartNotification.setVisibility(View.INVISIBLE);
            txtCartNotification.setVisibility(View.INVISIBLE);
            recCart.setVisibility(View.VISIBLE);
        }
    }
}