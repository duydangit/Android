package com.example.htdk_shop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.htdk_shop.R;
import com.example.htdk_shop.model.Category;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private Context context;
    private int layoutItemCategory;
    private ArrayList<Category> listCategory;

    public CategoryAdapter(Context context, int layoutItemCategory, ArrayList<Category> listCategory) {
        this.context = context;
        this.layoutItemCategory = layoutItemCategory;
        this.listCategory = listCategory;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imvItemCategory;
        TextView txtItemCategoryName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imvItemCategory = itemView.findViewById(R.id.imvItemCategory);
            txtItemCategoryName = itemView.findViewById(R.id.txtItemCategoryName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.getId();
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View categoryView = inflater.inflate(layoutItemCategory, parent, false);
        ViewHolder viewHolder = new ViewHolder(categoryView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Category category = listCategory.get(position);
        Picasso.with(context).load(category.getImage()).into(holder.imvItemCategory);
        holder.itemView.findViewById(R.id.txtItemCategoryName);
    }

    @Override
    public int getItemCount() {
        return listCategory.size();
    }

}
