package com.example.htdk_shop.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.htdk_shop.R;
import com.example.htdk_shop.activity.ProductDetailActivity;
import com.example.htdk_shop.model.Product;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class LaptopAdapter extends RecyclerView.Adapter<LaptopAdapter.Viewholder> {

    private Context context;
    private int layoutItemLaptop;
    private ArrayList<Product> listLaptop;

    public LaptopAdapter(Context context, int layoutItemLaptop, ArrayList<Product> listLaptop) {
        this.context = context;
        this.layoutItemLaptop = layoutItemLaptop;
        this.listLaptop = listLaptop;
    }
    public class Viewholder extends RecyclerView.ViewHolder {
        ImageView imvItemLaptop;
        TextView txtItemLaptopName, txtItemLaptopPrice, txtItemLaptopInfo;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            imvItemLaptop = itemView.findViewById(R.id.imvItemLaptop);
            txtItemLaptopName = itemView.findViewById(R.id.txtItemLaptopName);
            txtItemLaptopPrice = itemView.findViewById(R.id.txtItemLaptopPrice);
            txtItemLaptopInfo = itemView.findViewById(R.id.txtItemLaptopInfo);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Product product = listLaptop.get(getAdapterPosition());

                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra("Product", (Serializable) product);
                    context.startActivity(intent);
                }
            });
        }
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(layoutItemLaptop, parent, false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        Product product = listLaptop.get(position);

        Picasso.with(context).load(product.getImage()).into(holder.imvItemLaptop);
        holder.txtItemLaptopName.setText(product.getName());
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        holder.txtItemLaptopPrice.setText("Giá : " + decimalFormat.format(product.getPrice())+ " đ");
        holder.txtItemLaptopInfo.setText(product.getInfo());
    }

    @Override
    public int getItemCount() {
        return listLaptop.size();
    }

}
