package com.example.htdk_shop.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.htdk_shop.R;
import com.example.htdk_shop.activity.CartActivity;
import com.example.htdk_shop.activity.MainActivity;
import com.example.htdk_shop.model.Cart;
import com.example.htdk_shop.ultil.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.example.htdk_shop.activity.CartActivity.EventUltil;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.Viewholder> {

    private Context context;
    private int layoutItemCart;
    private ArrayList<Cart> listCart;

    public CartAdapter(Context context, int layoutItemCart, ArrayList<Cart> listCart) {
        this.context = context;
        this.layoutItemCart = layoutItemCart;
        this.listCart = listCart;
    }


    public class Viewholder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        ImageView imvItemCart;
        TextView txtItemCartName, txtItemCartPrice, txtItemCartQty;

        private ItemClickListener itemClickListener;

        ImageButton btnItemCartQtyDown, btnItemCartQtyUp;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            imvItemCart = itemView.findViewById(R.id.imvItemCart);
            txtItemCartName = itemView.findViewById(R.id.txtItemCartName);
            txtItemCartPrice = itemView.findViewById(R.id.txtItemCartPrice);
            txtItemCartQty = itemView.findViewById(R.id.txtItemCartQty);
            btnItemCartQtyDown = itemView.findViewById(R.id.btnItemCartQtyDown);
            btnItemCartQtyUp = itemView.findViewById(R.id.btnItemCartQtyUp);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public  void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view, getAdapterPosition(),false);
        }

        @Override
        public boolean onLongClick(View view) {
            itemClickListener.onClick(view, getAdapterPosition(), true);
            return true;
        }
    }
    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View cartView = inflater.inflate(layoutItemCart, parent,false);
        Viewholder viewholder = new Viewholder(cartView);
        return viewholder;
    }


    Cart cart;
    @Override
    public void onBindViewHolder(@NonNull final Viewholder holder, final int position) {
        cart = listCart.get(position);

        Picasso.with(context).load(cart.getProductImage()).into(holder.imvItemCart);
        holder.txtItemCartName.setText(cart.getProductName());
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        holder.txtItemCartPrice.setText(decimalFormat.format(cart.getProductPrice()) + " đ");
        holder.txtItemCartQty.setText(cart.getQty()+"");

        int qty= Integer.parseInt(holder.txtItemCartQty.getText().toString());
        checkQty(qty, holder.btnItemCartQtyUp, holder.btnItemCartQtyDown);

        holder.btnItemCartQtyUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cart.setQty(cart.getQty() + 1);
                checkQty(cart.getQty(), holder.btnItemCartQtyUp, holder.btnItemCartQtyDown);
                holder.txtItemCartQty.setText(cart.getQty() + "");
                EventUltil();
            }
        });

        holder.btnItemCartQtyDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cart.setQty(cart.getQty() - 1);
                checkQty(cart.getQty(), holder.btnItemCartQtyUp, holder.btnItemCartQtyDown);
                holder.txtItemCartQty.setText(cart.getQty() + "");
                EventUltil();
            }
        });
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, final int position, boolean isLongClick) {
                if(isLongClick){
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Xác nhận xóa sản phẩm");
                    builder.setMessage("Bạn có chắc muốn xóa sản phẩm này không?");
                    builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (MainActivity.manggiohang.size() <= 0) {
                                CartActivity.txtCartNotification.setVisibility(View.VISIBLE);
                            } else {
                                MainActivity.manggiohang.remove(position);
                                CartActivity.cartAdapter.notifyDataSetChanged();
                                EventUltil();
                                if (MainActivity.manggiohang.size() <= 0) {
                                    CartActivity.txtCartNotification.setVisibility(View.VISIBLE);
                                } else {
                                    CartActivity.txtCartNotification.setVisibility(View.INVISIBLE);
                                    CartActivity.cartAdapter.notifyDataSetChanged();
                                    EventUltil();
                                }
                            }
                        }
                    }).setNegativeButton("Không", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            CartActivity.cartAdapter.notifyDataSetChanged();
                            EventUltil();
                        }
                    });
                    builder.show();
                }else{
                    Toast.makeText(context, "Mã sản phẩm: "+listCart.get(position).getProductId(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listCart.size();
    }

    public void checkQty(int qty, ImageButton btnQtyUp, ImageButton btnQtyDown) {
        if (qty < 1) {
            btnQtyDown.setEnabled(false);
            btnQtyUp.setEnabled(true);
            cart.setQty(qty + 1);
            Toast toast = Toast.makeText(context, "Vui lòng chọn số lượng tối thiểu 1!",Toast.LENGTH_SHORT);
            toast.show();
        } else if (qty > 10) {
            btnQtyUp.setEnabled(false);
            btnQtyDown.setEnabled(true);
            cart.setQty(qty - 1);
            Toast toast = Toast.makeText(context, "Vui lòng chọn số lượng tối đa 10!",Toast.LENGTH_SHORT);
            toast.show();
        } else {
            btnQtyDown.setEnabled(true);
            btnQtyUp.setEnabled(true);
        }
    }
}